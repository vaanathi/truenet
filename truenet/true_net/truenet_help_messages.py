import pkg_resources

#=========================================================================================
# Truenet help, description and epilog messages to display
# Vaanathi Sundaresan
# 09-03-2021, Oxford
#=========================================================================================

def help_descs():
    version = pkg_resources.require("truenet")[0].version
    helps = {
        'mainparser':
        "truenet: Triplanar ensemble U-Net model, v" + str(version) + "\n" 
        "   \n" 
        "Sub-commands available:\n" 
        "       truenet train           Training a TrUE-Net model from scratch\n"
        "       truenet apply           Applying a saved/pretrained TrUE-Net model for testing\n"
        "       truenet fine_tune       Fine-tuning a saved/pretrained TrUE-Net model\n"
        "       truenet cross_validate  Cross-validation of TrUE-Net model\n"
        "   \n"
        "   \n"
        "For detailed help regarding the options for each command,\n"
        "type truenet <command> --help (e.g. truenet train --help)\n"
        "   \n",

        'train':
        '   \n'
        'truenet train: training the TrUE-Net model from scratch, v' + str(version) + '\n'
        '   \n'
        'Usage: truenet train -i <input_masterfile> -m <model_directory> [options]\n'
        '   \n'
        'Compulsory arguments:\n'
        '       -i, --inp_file                      Name of the masterfile with the absolute path\n'
        '       -m, --model_dir                     Directory for saving model weights\n'
        '   \n'
        'Optional arguments:\n'
        '       -tr_prop, --train_prop              Proportion of data used for training [0, 1]. The rest will be used for validation [default = 0.8]\n'
        '       -bfactor, --batch_factor            Number of subjects to be considered for each mini-epoch [default = 10]\n'
        '       -loss, --loss_function              Applying spatial weights to loss function. Options: weighted, nweighted [default=weighted]\n'
        '       -nclass, --num_classes              Number of classes to consider in the target labels (nclass=2 will consider only 0 and 1 in labels;\n'
        '                                           any additional class will be considered part of background class [default = 2]\n'
        '       -plane, --acq_plane                 The plane in which the model needs to be trained. Options: axial, sagittal, coronal, all [default = all]\n'
        '       -da, --data_augmentation            Applying data augmentation [default = True]\n'
        '       -af, --aug_factor                   Data inflation factor for augmentation [default = 2]\n'
        '       -ilr, --init_learng_rate            Initial LR to use in scheduler [0, 0.1] [default=0.001]\n'
        '       -lrm, --lr_sch_mlstone              Milestones for LR scheduler (e.g. -lrm 5 10 - to reduce LR at 5th and 10th epochs) [default = 10]\n'
        '       -gamma, --lr_sch_gamma              Factor by which the LR needs to be reduced in the LR scheduler [default = 0.1]\n'
        '       -opt, --optimizer                   Optimizer used for training. Options:adam, sgd [default = adam]\n'
        '       -eps, --epsilon                     Epsilon for adam optimiser (default=1e-4)\n'
        '       -mom, --momentum                    Momentum for sgd optimiser (default=0.9)'                                                                             
        '       -bs, --batch_size                   Batch size used for training [default = 8]\n'
        '       -ep, --num_epochs                   Number of epochs for training [default = 60]\n'
        '       -es, --early_stop_val               Number of epochs to wait for progress (early stopping) [default = 20]\n'
        '       -sv_resume, --save_resume_training  Whether to save and resume training in case of interruptions (default-False)\n'                                                                            
        '       -sv_mod, --save_full_model          Saving the whole model instead of weights alone [default = False]\n'
        '       -cv_type, --cp_save_type            Checkpoint to be saved. Options: best, last, everyN [default = last]\n'
        '       -cp_n, --cp_everyn_N                If -cv_type=everyN, the N value [default = 10]\n'
        '       -cpu, --use_cpu                     Force truenet to run on CPU (default=False)'                                                                             
        '       -v, --verbose                       Display debug messages [default = False]\n'
        '   \n',

        'apply':
        'truenet apply: testing the TrUE-Net model, v' + str(version) + '\n'
        '   \n'
        'Usage: truenet apply -i <input_masterfile> -m <model_name> -o <output_directory> [options]'
        '   \n'
        'Compulsory arguments:\n'
        '       -i, --inp_file                        Name of the masterfile with the absolute path\n'
        '       -m, --model_name                      Model basename with absolute path / '
        '                                                   pretrained model name (mwsc, mwsc_flair, mwsc_t1, ukbb, ukbb_flair, ukbb_t1)\n'                                                                  
        '       -o, --output_dir                      Path to the directory for saving output predictions\n'
        '   \n'
        'Optional arguments:\n'
        '       -int, --intermediate                  Saving intermediate prediction results (individual planes) for each subject [default = False]\n'
        '       -cpu, --use_cpu                       Force truenet to run on CPU (default=False)' 
        '       -v, --verbose                         Display debug messages [default = False]\n'
        '   \n',

        'fine_tune':
        'truenet fine_tune: training the TrUE-Net model from scratch, v' + str(version) + '\n'
        '   \n'
        'Usage: truenet fine_tune -i <input_masterfile> -m <model_name> -o <output_directory> [options]\n'
        '   \n'
        '   \n'
        'Compulsory arguments:\n'
        '       -i, --inp_file                        Name of the masterfile with the absolute path\n'
        '       -m, --model_name                      Model basename with absolute path / '
        '                                                   pretrained model name (mwsc, mwsc_flair, mwsc_t1, ukbb, ukbb_flair, ukbb_t1)\n'
        '       -o, --output_dir                      Output directory for saving fine-tuned models/weights\n'
        '   \n'
        'Optional arguments:\n'
        '       -ftlayers, --ft_layers                Layers to fine-tune starting from the decoder (e.g. 1 2 -> final two two decoder layers)\n'
        '       -tr_prop, --train_prop                Proportion of data used for fine-tuning [0, 1]. The rest will be used for validation [default = 0.8]\n'
        '       -bfactor, --batch_factor              Number of subjects to be considered for each mini-epoch [default = 10]\n'
        '       -loss, --loss_function                Applying spatial weights to loss function. Options: weighted, nweighted [default=weighted]\n'
        '       -nclass, --num_classes                Number of classes to consider in the target labels (nclass=2 will consider only 0 and 1 in labels; \n'
        '                                             any additional class will be considered part of background class [default = 2]\n'
        '       -plane, --acq_plane                   The plane in which the model needs to be fine-tuned. Options: axial, sagittal, coronal, all [default = all]\n'
        '       -da, --data_augmentation              Applying data augmentation [default = True]\n'
        '       -af, --aug_factor                     Data inflation factor for augmentation [default = 2]\n'
        '       -ilr, --init_learng_rate              Initial LR to use in scheduler for fine-tuning [0, 0.1] [default=0.0001]\n'
        '       -lrm, --lr_sch_mlstone                Milestones for LR scheduler (e.g. -lrm 5 10 - to reduce LR at 5th and 10th epochs) [default = 10]\n'
        '       -gamma, --lr_sch_gamma                Factor by which the LR needs to be reduced in the LR scheduler [default = 0.1]\n'
        '       -opt, --optimizer                     Optimizer used for fine-tuning. Options:adam, sgd [default = adam]\n'
        '       -eps, --epsilon                       Epsilon for adam optimiser (default=1e-4)\n'
        '       -mom, --momentum                      Momentum for sgd optimiser (default=0.9)'                                                                             
        '       -bs, --batch_size                     Batch size used for fine-tuning [default = 8]\n'
        '       -ep, --num_epochs                     Number of epochs for fine-tuning [default = 60]\n'
        '       -es, --early_stop_val                 Number of fine-tuning epochs to wait for progress (early stopping) [default = 20]\n'
        '       -sv_resume, --save_resume_training    Whether to save and resume training in case of interruptions (default-False)\n'                                                                            
        '       -sv_mod, --save_full_model            Saving the whole fine-tuned model instead of weights alone [default = False]\n'
        '       -cv_type, --cp_save_type              Checkpoint to be saved. Options: best, last, everyN [default = last]\n'
        '       -cp_n, --cp_everyn_N                  If -cv_type = everyN, the N value [default = 10]\n'
        '       -cpu, --use_cpu                       Force truenet to run on CPU (default=False)'                                                                             
        '       -v, --verbose                         Display debug messages [default = False]\n'
        '   \n',

        'cross_validate':
        'truenet cross_validate: cross-validation of the TrUE-Net model, v' + str(version) + '\n'                                                                                            
        '   \n'
        'Usage: truenet cross_validate -i <input_masterfile> -o <output_directory> [options]\n'
        '   \n'
        '   \n'
        'Compulsory arguments:\n'
        '       -i, --inp_file                         Name of the masterfile with the absolute path\n'
        '       -o, --output_dir                      Output directory for saving predictions (and models)\n'
        '   \n'
        'Optional arguments:\n'
        '       -fold, --cv_fold                      Number of folds for crossvalidation (default = 5)\n' 
        '       -resume_fold, --resume_from_fold      Resume crossvalidation from the specified fold (default = 1)\n'                                                                                    
        '       -tr_prop, --train_prop                Proportion of data used for training [0, 1]. The rest will be used for validation [default = 0.8]\n'
        '       -bfactor, --batch_factor              Number of subjects to be considered for each mini-epoch [default = 10]\n'
        '       -loss, --loss_function                Applying spatial weights to loss function. Options: weighted, nweighted [default=weighted]\n'
        '       -nclass, --num_classes                Number of classes to consider in the target labels (nclass=2 will consider only 0 and 1 in labels; \n'
        '                                             any additional class will be considered part of background class [default = 2]\n'
        '       -plane, --acq_plane                   The plane in which the model needs to be trained. Options: axial, sagittal, coronal, all [default = all]\n'
        '       -da, --data_augmentation              Applying data augmentation [default = True]\n'
        '       -af, --aug_factor                     Data inflation factor for augmentation [default = 2]\n'
        '       -ilr, --init_learng_rate              Initial LR to use in scheduler for training [0, 0.1] [default=0.0001]\n'
        '       -lrm, --lr_sch_mlstone                Milestones for LR scheduler (e.g. -lrm 5 10 - to reduce LR at 5th and 10th epochs) [default = 10]\n'
        '       -gamma, --lr_sch_gamma                Factor by which the LR needs to be reduced in the LR scheduler [default = 0.1]\n'
        '       -opt, --optimizer                     Optimizer used for training. Options:adam, sgd [default = adam]\n'
        '       -eps, --epsilon                       Epsilon for adam optimiser (default=1e-4)\n'
        '       -mom, --momentum                      Momentum for sgd optimiser (default=0.9)'                                                                             
        '       -bs, --batch_size                     Batch size used for fine-tuning [default = 8]\n'
        '       -ep, --num_epochs                     Number of epochs for fine-tuning [default = 60]\n'
        '       -es, --early_stop_val                 Number of fine-tuning epochs to wait for progress (early stopping) [default = 20]\n'
        '       -int, --intermediate                  Saving intermediate prediction results (individual planes) for each subject [default = False]\n'                                                                                     
        '       -sv_resume, --save_resume_training    Whether to save and resume training in case of interruptions (default-False)\n'                                                                            
        '       -sv_mod, --save_full_model            Saving the whole fine-tuned model instead of weights alone [default = False]\n'
        '       -cv_type, --cp_save_type              Checkpoint to be saved. Options: best, last, everyN [default = last]\n'
        '       -cp_n, --cp_everyn_N                  If -cv_type = everyN, the N value [default = 10]\n'
        '       -cpu, --use_cpu                       Force truenet to run on CPU (default=False)'                                                                             
        '       -v, --verbose                         Display debug messages [default = False]\n'
        '   \n'
    }
    return helps


def desc_descs():
    version = pkg_resources.require("truenet")[0].version
    descs = {
        'mainparser' :
        "truenet: Triplanar ensemble U-Net model, v" + str(version) + "\n"
        "   \n"
        "Sub-commands available:\n"
        "       truenet train           Training a TrUE-Net model from scratch\n"
        "       truenet apply        Applying a saved/pretrained TrUE-Net model for testing\n"
        "       truenet fine_tune       Fine-tuning a saved/pretrained TrUE-Net model \n"
        "       truenet cross_validate  Cross-validation of TrUE-Net model\n"
        "   \n",

        'train':
        '   \n'
        'truenet: Triplanar ensemble U-Net model, v' + str(version) + '\n'
        '   \n'                                                             
        'The \'train\' command trains the TrUE-Net model from scratch using the training subjects specified in\n'
        'the input masterfile. Look up \'prepare_truenet_data\' for creating a masterfile. The masterfile should have\n'
        'headers such as FLAIR T1 manualmask ventdistmap GMdistmap (as required) and filepaths in the corresponding \n'
        'order for one subject per row\n'
        '   \n',

        'apply':
        '   \n'
        'truenet: Triplanar ensemble U-Net model, v' + str(version) + '\n'
        '   \n'                                                             
        'The \'apply\' command is used for testing the TrUE-Net model on the test subjects specified in\n'
        'the input masterfile. Look up \'prepare_truenet_data\' for creating a masterfile. The masterfile should have\n'
        'headers such as FLAIR T1 manualmask ventdistmap GMdistmap (as required) and filepaths in the corresponding \n'
        'order for one subject per row\n'
        '   \n',

        'fine_tune':
        '   \n'
        'truenet: Triplanar ensemble U-Net model, v' + str(version) + '\n'
        '   \n'
        'The \'fine_tune\' command fine-tunes a pretrained TrUE-Net model (from a model directory) on the\n'
        'training subjects in the input masterfile. Look up \'prepare_truenet_data\' for creating a masterfile. \n'
        'The masterfile should have headers such as FLAIR T1 manualmask ventdistmap GMdistmap (as required) \n'
        'and filepaths in the corresponding order for one subject per row\n'
        '   \n',

        'cross_validate':
        '   \n'
        'truenet: Triplanar ensemble U-Net model, v' + str(version) + '\n'
        '   \n'
        'The \'cross_validate\' command performs cross-validation of the TrUE-Net model on the\n'
        'subjects in the input masterfile. Look up \'prepare_truenet_data\' for creating a masterfile. \n'
        'The masterfile should have headers such as FLAIR T1 manualmask ventdistmap GMdistmap (as required) \n'
        ' and filepaths in the corresponding order for one subject per row\n'
        '   \n'
    }
    return descs


def epilog_descs():
    epilogs = {
        'mainparser' :
        "   \n"
        "For detailed help regarding the options for each command,\n"
        "type truenet <command> --help or -h (e.g. truenet train --help, truenet train -h)\n"
        "   \n",

        'subparsers' :
        '   \n'
        "For detailed help regarding the options for each argument,\n"
        "refer to the user-guide or readme document. For more details on\n"
        "TrUE-Net, refer to https://www.biorxiv.org/content/10.1101/2020.07.24.219485v1.full\n"
        "   \n",
    }
    return epilogs

