from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
from truenet.true_net import (truenet_train_function, truenet_test_function,
                              truenet_cross_validate, truenet_finetune)
import glob
import numpy as np

#=========================================================================================
# Truenet commands function
# Vaanathi Sundaresan
# 10-03-2021, Oxford
#=========================================================================================


##########################################################################################
# Define the train sub-command for truenet
##########################################################################################

def train(args):
    """
    :param args: Input arguments from argparse
    """
    # Do basic sanity checks and assign variable names
    if os.path.isdir(args.model_dir) is False:
        raise ValueError(args.model_dir + ' does not appear to be a valid directory')
    model_dir = args.model_dir

    if os.path.isfile(args.inp_file):
        masterfile = args.inp_file
    else:
        raise ValueError(args.inp_file + 'is not a valid filename')

    if isinstance(args.init_learng_rate, float) is False:
        raise ValueError('Initial learning rate must be a float value')
    else:
        if args.init_learng_rate > 1:
            raise ValueError('Initial learning rate must be between 0 and 1')

    if args.optimizer not in ['adam', 'sgd']:
        raise ValueError('Invalid option for Optimizer: Valid options: adam, sgd')

    if args.acq_plane not in ['axial', 'sagittal', 'coronal', 'all']:
        raise ValueError('Invalid option for acquisition plane: Valid options: axial, sagittal, coronal, all')

    if isinstance(args.lr_sch_gamma, float) is False:
        raise ValueError('Learning rate reduction factor must be a float value')
    else:
        if args.lr_sch_gamma > 1:
            raise ValueError('Learning rate reduction factor must be between 0 and 1')

    if isinstance(args.train_prop, float) is False:
        raise ValueError('Training data proportion must be a float value')
    else:
        if args.train_prop > 1:
            raise ValueError('Training data proportion must be between 0 and 1')

    if args.batch_size < 1:
        raise ValueError('Batch size must be an int and > 1')
    if args.num_epochs < 1:
        raise ValueError('Number of epochs must be an int and > 1')
    if args.batch_factor < 1:
        raise ValueError('Batch factor must be an int and > 1')
    if args.early_stop_val < 1 or args.early_stop_val > args.num_epochs:
        raise ValueError('Early stopping patience value must be an int and > 1 and < number of epochs')
    if args.aug_factor < 1:
        raise ValueError('Augmentation factor must be an int and > 1')
    if args.cp_save_type == 'everyN':
        if args.cp_everyn_N < 1 or args.cp_everyn_N > args.num_epochs:
            raise ValueError(
                'N value for saving checkpoints for every N epochs must be an int and > 1and < number of epochs')

    if args.num_classes < 1:
        raise ValueError('Number of classes to consider in target segmentations must be an int and > 1')

    with open(masterfile, 'r') as mfile:
        masterfile_content = (line.rstrip() for line in mfile)  # All lines including the blank ones
        masterfile_content = [line for line in masterfile_content if line]  # Removes blank lines

    header = masterfile_content[0]
    masterfile_content = masterfile_content[1:]

    keyword_dict = {'flair_kwords':['FLAIR', 'flair', 'Flair'],
                    't1_kwords': ['T1', 't1'],
                    'label_kwords': ['manualmask', 'manual_mask', 'Manualmask', 'Manual_mask', 'ManualMask'],
                    'ventdist_kwords': ['ventdistmap', 'ventdist_map', 'Ventdist_map', 'vent_dist_map',
                       'VentDistMap', 'Vent_dist_map'],
                    'GMdist_kwords': ['GMdistmap', 'GMdist_map', 'gmdist_map', 'GM_dist_map', 'gm_dist_map', 'GMDistMap']}

    keyword_keys = ['flair_kwords', 't1_kwords', 'label_kwords', 'ventdist_kwords', 'GMdist_kwords']

    # Order: FLAIR, T1, label, ventdist, GMdist
    inp_flags = [0, 0, 0, 0, 0]
    pos_array = [-1, -1, -1, -1, -1]

    input_kwords = header.split(' ')
    for column_id, kword in enumerate(input_kwords):
        for ki in range(5):
            if kword in keyword_dict[keyword_keys[ki]]:
                inp_flags[ki] += 1
                pos_array[ki] = column_id

    if any(inp_flags) > 1:
        raise ValueError('Keywords cannot be repeated in the header of the Masterfile')

    if inp_flags[0] == 0 and inp_flags[1] == 0:
        raise ValueError('At least FLAIR or T1 must be provided in the masterfile')

    numchannels = inp_flags[0] + inp_flags[1]

    if inp_flags[2] == 0:
        raise ValueError('Manualmask keyword is mandatory for training truenet.')

    if args.loss_function == 'weighted':
        weighted = True
    else:
        weighted = False

    if args.loss_function == 'weighted':
        if inp_flags[3] == 0 or inp_flags[4] == 0:
            raise ValueError('Masterfile must have both GMdistmap and ventdistmap keywords if '
                             '-loss is "weighted"!')

    filepath_keys = ['flair_path', 't1_path', 'gt_path', 'ventdist_path', 'gmdist_path', 'basename']

    files_indices = np.where(np.array(inp_flags) > 0)[0]

    # Create a list of dictionaries containing required filepaths for the training subjects
    subj_name_dicts = []
    for line in range(len(masterfile_content)):
        paths = masterfile_content[line].split(' ')
        subj_name_dict = dict.fromkeys(filepath_keys)
        if len(paths) != files_indices.shape[0]:
            raise ValueError('No. of columns in line ' + str(line + 2) + ' does not match no. of keywords in the Masterfile header')
        count = 0
        for idx in range(5):
            if inp_flags[idx] > 0:
                if os.path.isfile(paths[count]):
                    subj_name_dict[filepath_keys[idx]] = paths[pos_array[idx]]
                    count += 1
                else:
                    raise ValueError(paths[count] + 'is not a valid filename')
            else:
                subj_name_dict[filepath_keys[idx]] = None
        if inp_flags[0] > 1:
            basename = os.path.basename(paths[pos_array[0]]).split('.')[0]
        else:
            basename = os.path.basename(paths[pos_array[1]]).split('.')[0]
        subj_name_dict['basename'] = basename
        subj_name_dicts.append(subj_name_dict)

    # Create the training parameters dictionary
    training_params = {'Learning_rate': args.init_learng_rate,
                       'Optimizer': args.optimizer,
                       'Epsilon' : args.epsilon,
                       'Momentum' : args.momentum,
                       'LR_Milestones': args.lr_sch_mlstone,
                       'LR_red_factor': args.lr_sch_gamma,
                       'Acq_plane': args.acq_plane,
                       'Num_channels': numchannels,
                       'Train_prop': args.train_prop,
                       'Batch_size': args.batch_size,
                       'Num_epochs': args.num_epochs,
                       'Batch_factor': args.batch_factor,
                       'Patience': args.early_stop_val,
                       'Aug_factor': args.aug_factor,
                       'EveryN': args.cp_everyn_N,
                       'Nclass': args.num_classes,
                       'SaveResume': args.save_resume_training,
                       'Use_CPU': args.use_cpu
                       }

    if args.save_full_model == 'True':
        save_wei = False
    else:
        save_wei = True

    if args.cp_save_type not in ['best', 'last', 'everyN']:
        raise ValueError('Invalid option for checkpoint save type: Valid options: best, last, everyN')

    # Training main function call
    models = truenet_train_function.main(subj_name_dicts, training_params, aug=args.data_augmentation, weighted=weighted,
         save_cp=True, save_wei=save_wei, save_case=args.cp_save_type, verbose=args.verbose, dir_cp=model_dir)


##########################################################################################
# Define the apply sub-command for truenet
##########################################################################################

def apply(args):
    '''
    :param args: Input arguments from argparse
    '''
    # Do basic sanity checks and assign variable names
    out_dir = args.output_dir
    if os.path.isdir(out_dir) is False:
        raise ValueError(out_dir + ' does not appear to be a valid directory')

    if os.path.isfile(args.inp_file):
        masterfile = args.inp_file
    else:
        raise ValueError(args.inp_file + 'is not a valid filename')

    with open(masterfile, 'r') as mfile:
        masterfile_content = (line.rstrip() for line in mfile)  # All lines including the blank ones
        masterfile_content = [line for line in masterfile_content if line]  # Removes blank lines

    header = masterfile_content[0]
    masterfile_content = masterfile_content[1:]

    keyword_dict = {'flair_kwords': ['FLAIR', 'flair', 'Flair'],
                    't1_kwords': ['T1', 't1'],
                    'label_kwords': ['manualmask', 'manual_mask', 'Manualmask', 'Manual_mask', 'ManualMask'],
                    'ventdist_kwords': ['ventdistmap', 'ventdist_map', 'Ventdist_map', 'vent_dist_map',
                                        'VentDistMap', 'Vent_dist_map'],
                    'GMdist_kwords': ['GMdistmap', 'GMdist_map', 'gmdist_map', 'GM_dist_map', 'gm_dist_map',
                                      'GMDistMap']}

    keyword_keys = ['flair_kwords', 't1_kwords', 'label_kwords', 'ventdist_kwords', 'GMdist_kwords']

    # Order: FLAIR, T1, label, ventdist, GMdist
    inp_flags = [0, 0, 0, 0, 0]
    pos_array = [-1, -1, -1, -1, -1]

    input_kwords = header.split(' ')
    for column_id, kword in enumerate(input_kwords):
        for ki in range(5):
            if kword in keyword_dict[keyword_keys[ki]]:
                inp_flags[ki] += 1
                pos_array[ki] = column_id

    if any(inp_flags) > 1:
        raise ValueError('Keywords cannot be repeated in the header of the Masterfile')

    if inp_flags[0] == 0 and inp_flags[1] == 0:
        raise ValueError('At least FLAIR or T1 must be provided in the masterfile')

    if args.model_name == 'mwsc' or args.model_name == 'mwsc_flair' or args.model_name == 'ukbb' or args.model_name == 'ukbb_flair':
        if inp_flags[0] == 0:
            raise ValueError('FLAIR keyword is mandatory in the header for ' + args.model_name + ' model')

    if args.model_name == 'mwsc' or args.model_name == 'mwsc_t1' or args.model_name == 'ukbb' or args.model_name == 'ukbb_t1':
        if inp_flags[1] == 0:
            raise ValueError('T1 keyword is mandatory in the header for ' + args.model_name + ' model')

    filepath_keys = ['flair_path', 't1_path', 'gt_path', 'ventdist_path', 'gmdist_path', 'basename']

    files_indices = np.where(np.array(inp_flags) > 0)[0]

    # Create a list of dictionaries containing required filepaths for the test subjects
    subj_name_dicts = []
    for line in range(len(masterfile_content)):
        paths = masterfile_content[line].split(' ')
        subj_name_dict = dict.fromkeys(filepath_keys)
        if len(paths) != files_indices.shape[0]:
            raise ValueError(
                'No. of columns in line ' + str(line + 2) + ' does not match no. of keywords in the Masterfile header')
        count = 0
        for idx in range(5):
            if inp_flags[idx] > 0:
                if os.path.isfile(paths[count]):
                    subj_name_dict[filepath_keys[idx]] = paths[pos_array[idx]]
                    count += 1
                else:
                    raise ValueError(paths[idx] + 'is not a valid filename')
            else:
                subj_name_dict[filepath_keys[idx]] = None
        if inp_flags[0] > 1:
            basename = os.path.basename(paths[pos_array[0]]).split('.')[0]
        else:
            basename = os.path.basename(paths[pos_array[1]]).split('.')[0]
        subj_name_dict['basename'] = basename
        del subj_name_dict['gt_path']
        subj_name_dicts.append(subj_name_dict)

    pretrained = 0
    if args.model_name == 'mwsc' or args.model_name == 'mwsc_flair' or args.model_name == 'mwsc_t1':
        pretrained = 1
        model_dir = os.path.expandvars('$FSLDIR/data/truenet/models/mwsc')
        if not os.path.exists(model_dir):
            model_dir = os.environ.get('TRUENET_PRETRAINED_MODEL_PATH', None)
            if model_dir is None:
                raise RuntimeError('Cannot find data; Download the available models and '
                                   'export TRUENET_PRETRAINED_MODEL_PATH=/path/to/my/mwsc/model')
        if args.model_name == 'mwsc':
            print('Models pretrained on MWSC, using FLAIR and T1 as input channels are chosen. Proceeding ...',
                  flush=True)
            model_name = 'Truenet_MWSC_FLAIR_T1'
            numchannels = 2
        elif args.model_name == 'mwsc_flair':
            print('Models pretrained on MWSC, using only FLAIR as input channel are chosen. Proceeding ...',
                  flush=True)
            model_name = 'Truenet_MWSC_FLAIR'
            numchannels = 1
        elif args.model_name == 'mwsc_t1':
            print('Models pretrained on MWSC, using only T1 as input channel are chosen. Proceeding ...',
                  flush=True)
            model_name = 'Truenet_MWSC_T1'
            numchannels = 1
    elif args.model_name == 'ukbb' or args.model_name == 'ukbb_flair' or args.model_name == 'ukbb_t1':
        pretrained = 1
        model_dir = os.path.expandvars('$FSLDIR/data/truenet/models/ukbb')
        if not os.path.exists(model_dir):
            model_dir = os.environ.get('TRUENET_PRETRAINED_MODEL_PATH', None)
            if model_dir is None:
                raise RuntimeError('Cannot find data; Download the available models and '
                                   'export TRUENET_PRETRAINED_MODEL_PATH=/path/to/my/ukbb/model')
        if args.model_name == 'ukbb':
            print('Models pretrained on UK Biobank, using FLAIR and T1 as input channels are chosen. Proceeding ...',
                  flush=True)
            model_name = 'Truenet_UKBB_FLAIR_T1'
            numchannels = 2
        elif args.model_name == 'ukbb_flair':
            print('Models pretrained on UK Biobank, using only FLAIR as input channel are chosen. Proceeding ...',
                  flush=True)
            model_name = 'Truenet_UKBB_FLAIR'
            numchannels = 1
        elif args.model_name == 'ukbb_t1':
            print('Models pretrained on UK Biobank, using only T1 as input channel are chosen. Proceeding ...',
                  flush=True)
            model_name = 'Truenet_UKBB_T1'
            numchannels = 1
    elif os.path.isfile(args.model_name + '_axial.pth') is True and \
                os.path.isfile(args.model_name + '_sagittal.pth') is True and \
                os.path.isfile(args.model_name + '_coronal.pth') is True:
        print('Found pretrained models in the specified path. Proceeding ...')
        model_dir = os.path.dirname(args.model_name)
        model_name = os.path.basename(args.model_name)
        numchannels = inp_flags[0] + inp_flags[1]
    else:
        raise ValueError('In directory ' + os.path.dirname(args.model_name) +
                         ', ' + os.path.basename(args.model_name) + '_axial.pth or ' +
                         os.path.basename(args.model_name) + '_sagittal.pth or ' +
                         os.path.basename(args.model_name) + '_coronal.pth ' +
                         'does not appear to be a valid model file. Please check the model filename. '
                         'Alternatively, you can also provide following standard pretrained model names:'
                         'mwsc, mwsc_flair, mwsc_t1, ukbb, ukbb_flair, ukbb_t1')

    # Create the training parameters dictionary
    eval_params = {'Pretrained': pretrained,
                   'Modelname': model_name,
                   'Use_CPU': args.use_cpu,
                   'Num_channels': numchannels
                   }

    # Test main function call
    truenet_test_function.main(subj_name_dicts, eval_params, intermediate=args.intermediate,
                               model_dir=model_dir, output_dir=out_dir, verbose=args.verbose)


##########################################################################################
# Define the fine_tune sub-command for truenet
##########################################################################################

def fine_tune(args):
    '''
    :param args: Input arguments from argparse
    '''
    # Do the usual sanity checks
    if os.path.isdir(args.output_dir) is False:
        raise ValueError(args.output_dir + ' does not appear to be a valid directory')
    out_dir = args.output_dir

    if isinstance(args.init_learng_rate, float) is False:
        raise ValueError('Initial learning rate must be a float value')
    else:
        if args.init_learng_rate > 1:
            raise ValueError('Initial learning rate must be between 0 and 1')

    if args.optimizer not in ['adam', 'sgd']:
        raise ValueError('Invalid option for Optimizer: Valid options: adam, sgd')

    if args.acq_plane not in ['axial', 'sagittal', 'coronal', 'all']:
        raise ValueError('Invalid option for acquisition plane: Valid options: axial, sagittal, coronal, all')

    if isinstance(args.lr_sch_gamma, float) is False:
        raise ValueError('Learning rate reduction factor must be a float value')
    else:
        if args.lr_sch_gamma > 1:
            raise ValueError('Learning rate reduction factor must be between 0 and 1')

    if isinstance(args.train_prop, float) is False:
        raise ValueError('Training data proportion must be a float value')
    else:
        if args.train_prop > 1:
            raise ValueError('Training data proportion must be between 0 and 1')

    if args.batch_size < 1:
        raise ValueError('Batch size must be an int and > 1')
    if args.num_epochs < 1:
        raise ValueError('Number of epochs must be an int and > 1')
    if args.batch_factor < 1:
        raise ValueError('Batch factor must be an int and > 1')
    if args.early_stop_val < 1 or args.early_stop_val > args.num_epochs:
        raise ValueError('Early stopping patience value must be an int and > 1 and < number of epochs')
    if args.aug_factor < 1:
        raise ValueError('Augmentation factor must be an int and > 1')

    if args.cp_save_type == 'everyN':
        if args.cp_everyn_N < 1 or args.cp_everyn_N > args.num_epochs:
            raise ValueError(
                'N value for saving checkpoints for every N epochs must be an int and > 1and < number of epochs')
    if args.num_classes < 1:
        raise ValueError('Number of classes to consider in target segmentations must be an int and > 1')

    if args.save_full_model == 'True':
        save_wei = False
    else:
        save_wei = True

    if os.path.isfile(args.inp_file):
        masterfile = args.inp_file
    else:
        raise ValueError(args.inp_file + 'is not a valid filename')

    with open(masterfile, 'r') as mfile:
        masterfile_content = (line.rstrip() for line in mfile)  # All lines including the blank ones
        masterfile_content = [line for line in masterfile_content if line]  # Removes blank lines

    header = masterfile_content[0]
    masterfile_content = masterfile_content[1:]

    keyword_dict = {'flair_kwords':['FLAIR', 'flair', 'Flair'],
                    't1_kwords': ['T1', 't1'],
                    'label_kwords': ['manualmask', 'manual_mask', 'Manualmask', 'Manual_mask', 'ManualMask'],
                    'ventdist_kwords': ['ventdistmap', 'ventdist_map', 'Ventdist_map', 'vent_dist_map',
                       'VentDistMap', 'Vent_dist_map'],
                    'GMdist_kwords': ['GMdistmap', 'GMdist_map', 'gmdist_map', 'GM_dist_map', 'gm_dist_map', 'GMDistMap']}

    keyword_keys = ['flair_kwords', 't1_kwords', 'label_kwords', 'ventdist_kwords', 'GMdist_kwords']

    # Order: FLAIR, T1, label, ventdist, GMdist
    inp_flags = [0, 0, 0, 0, 0]
    pos_array = [-1, -1, -1, -1, -1]

    input_kwords = header.split(' ')
    for column_id, kword in enumerate(input_kwords):
        for ki in range(5):
            if kword in keyword_dict[keyword_keys[ki]]:
                inp_flags[ki] += 1
                pos_array[ki] = column_id

    if any(inp_flags) > 1:
        raise ValueError('Keywords cannot be repeated in the header of the Masterfile')

    if inp_flags[0] == 0 and inp_flags[1] == 0:
        raise ValueError('At least FLAIR or T1 must be provided in the masterfile')

    if inp_flags[2] == 0:
        raise ValueError('Manualmask keyword is mandatory for fine-tuning truenet.')

    if args.loss_function == 'weighted':
        weighted = True
    else:
        weighted = False

    if args.loss_function == 'weighted':
        if inp_flags[3] == 0 or inp_flags[4] == 0:
            raise ValueError('Masterfile must have both GMdistmap and ventdistmap keywords if '
                             '-loss is "weighted"!')

    filepath_keys = ['flair_path', 't1_path', 'gt_path', 'ventdist_path', 'gmdist_path', 'basename']

    files_indices = np.where(np.array(inp_flags) > 0)[0]

    # Create a list of dictionaries containing required filepaths for the fine-tuning subjects
    subj_name_dicts = []
    for line in range(len(masterfile_content)):
        paths = masterfile_content[line].split(' ')
        print(paths)
        subj_name_dict = dict.fromkeys(filepath_keys)
        if len(paths) != files_indices.shape[0]:
            raise ValueError('No. of columns in line ' + str(line + 2) + ' does not match no. of keywords in the Masterfile header')
        count = 0
        for idx in range(5):
            if inp_flags[idx] > 0:
                if os.path.isfile(paths[count]):
                    subj_name_dict[filepath_keys[idx]] = paths[pos_array[idx]]
                    count += 1
                else:
                    raise ValueError(paths[idx] + 'is not a valid filename')
            else:
                subj_name_dict[filepath_keys[idx]] = None
        if inp_flags[0] > 1:
            basename = os.path.basename(paths[pos_array[0]]).split('.')[0]
        else:
            basename = os.path.basename(paths[pos_array[1]]).split('.')[0]
        subj_name_dict['basename'] = basename
        subj_name_dicts.append(subj_name_dict)

    pretrained = 0
    if args.model_name == 'mwsc' or args.model_name == 'mwsc_flair' or args.model_name == 'mwsc_t1':
        pretrained = 1
        model_dir = os.path.expandvars('$FSLDIR/data/truenet/models/mwsc')
        if not os.path.exists(model_dir):
            model_dir = os.environ.get('TRUENET_PRETRAINED_MODEL_PATH', None)
            if model_dir is None:
                raise RuntimeError('Cannot find data; Download the available models and '
                                   'export TRUENET_PRETRAINED_MODEL_PATH=/path/to/my/mwsc/model')
        if args.model_name == 'mwsc':
            print('Models pretrained on MWSC, using FLAIR and T1 as input channels are chosen. Proceeding ...',
                  flush=True)
            model_name = 'Truenet_MWSC_FLAIR_T1'
            numchannels = 2
        elif args.model_name == 'mwsc_flair':
            print('Models pretrained on MWSC, using only FLAIR as input channel are chosen. Proceeding ...',
                  flush=True)
            model_name = 'Truenet_MWSC_FLAIR'
            numchannels = 1
        elif args.model_name == 'mwsc_t1':
            print('Models pretrained on MWSC, using only T1 as input channel are chosen. Proceeding ...',
                  flush=True)
            model_name = 'Truenet_MWSC_T1'
            numchannels = 1
    elif args.model_name == 'ukbb' or args.model_name == 'ukbb_flair' or args.model_name == 'ukbb_t1':
        pretrained = 1
        model_dir = os.path.expandvars('$FSLDIR/data/truenet/models/ukbb')
        if not os.path.exists(model_dir):
            model_dir = os.environ.get('TRUENET_PRETRAINED_MODEL_PATH', None)
            if model_dir is None:
                raise RuntimeError('Cannot find data; Download the available models and '
                                   'export TRUENET_PRETRAINED_MODEL_PATH=/path/to/my/ukbb/model')
        if args.model_name == 'ukbb':
            print('Models pretrained on UK Biobank, using FLAIR and T1 as input channels are chosen. Proceeding ...',
                  flush=True)
            model_name = 'Truenet_UKBB_FLAIR_T1'
            numchannels = 2
        elif args.model_name == 'ukbb_flair':
            print('Models pretrained on UK Biobank, using only FLAIR as input channel are chosen. Proceeding ...',
                  flush=True)
            model_name = 'Truenet_UKBB_FLAIR'
            numchannels = 1
        elif args.model_name == 'ukbb_t1':
            print('Models pretrained on UK Biobank, using only T1 as input channel are chosen. Proceeding ...',
                  flush=True)
            model_name = 'Truenet_UKBB_T1'
            numchannels = 1
    elif os.path.isfile(args.model_name + '_axial.pth') is True and \
            os.path.isfile(args.model_name + '_sagittal.pth') is True and \
            os.path.isfile(args.model_name + '_coronal.pth') is True:
        print('Found pretrained models in the specified path. Proceeding ...')
        model_dir = os.path.dirname(args.model_name)
        model_name = os.path.basename(args.model_name)
        numchannels = inp_flags[0] + inp_flags[1]
    else:
        raise ValueError('In directory ' + os.path.dirname(args.model_name) +
                         ', ' + os.path.basename(args.model_name) + '_axial.pth or ' +
                         os.path.basename(args.model_name) + '_sagittal.pth or ' +
                         os.path.basename(args.model_name) + '_coronal.pth ' +
                         'does not appear to be a valid model file. Please check the model filename. '
                         'Alternatively, you can also provide following standard pretrained model names:'
                         'mwsc, mwsc_flair, mwsc_t1, ukbb, ukbb_flair, ukbb_t1')

    # Create the fine-tuning parameters dictionary
    finetuning_params = {'Finetuning_learning_rate': args.init_learng_rate,
                         'Optimizer': args.optimizer,
                         'Epsilon': args.epsilon,
                         'Momentum': args.momentum,
                         'LR_Milestones': args.lr_sch_mlstone,
                         'LR_red_factor': args.lr_sch_gamma,
                         'Acq_plane': args.acq_plane,
                         'Num_channels': numchannels,
                         'Train_prop': args.train_prop,
                         'Batch_size': args.batch_size,
                         'Num_epochs': args.num_epochs,
                         'Batch_factor': args.batch_factor,
                         'Patience': args.early_stop_val,
                         'Aug_factor': args.aug_factor,
                         'EveryN': args.cp_everyn_N,
                         'Nclass': args.num_classes,
                         'Finetuning_layers': args.ft_layers,
                         'Pretrained': pretrained,
                         'Modelname': model_name,
                         'SaveResume': args.save_resume_training,
                         'Use_CPU': args.use_cpu
                         }

    if args.cp_save_type not in ['best', 'last', 'everyN']:
        raise ValueError('Invalid option for checkpoint save type: Valid options: best, last, everyN')

    if args.cp_save_type == 'everyN':
        if args.cp_everyn_N is None:
            raise ValueError('-cp_n must be provided to specify the epoch for saving CP when -cp_type is "everyN"!')

    # Fine-tuning main function call
    truenet_finetune.main(subj_name_dicts, finetuning_params, aug=args.data_augmentation, weighted=weighted,
                          save_cp=True, save_wei=save_wei, save_case=args.cp_save_type, verbose=args.verbose,
                          model_dir=model_dir, dir_cp=out_dir)

##########################################################################################
# Define the loo_validate (leave-one-out validation) sub-command for truenet
##########################################################################################


def cross_validate(args):
    '''
    :param args: Input arguments from argparse
    '''
    # Usual sanity check for checking if filepaths and files exist.
    if os.path.isdir(args.output_dir) is False:
        raise ValueError(args.output_dir + ' does not appear to be a valid directory')
    out_dir = args.output_dir

    if os.path.isfile(args.inp_file):
        masterfile = args.inp_file
    else:
        raise ValueError(args.inp_file + 'is not a valid filename')

    if args.cv_fold < 1:
        raise ValueError('Number of folds cannot be 0 or negative')

    if args.resume_from_fold < 1:
        raise ValueError('Fold to resume cannot be 0 or negative')

    if isinstance(args.init_learng_rate, float) is False:
        raise ValueError('Initial learning rate must be a float value')
    else:
        if args.init_learng_rate > 1:
            raise ValueError('Initial learning rate must be between 0 and 1')

    if args.optimizer not in ['adam', 'sgd']:
        raise ValueError('Invalid option for Optimizer: Valid options: adam, sgd')

    if args.acq_plane not in ['axial', 'sagittal', 'coronal', 'all']:
        raise ValueError('Invalid option for acquisition plane: Valid options: axial, sagittal, coronal, all')

    if isinstance(args.lr_sch_gamma, float) is False:
        raise ValueError('Learning rate reduction factor must be a float value')
    else:
        if args.lr_sch_gamma > 1:
            raise ValueError('Learning rate reduction factor must be between 0 and 1')

    if isinstance(args.train_prop, float) is False:
        raise ValueError('Training data proportion must be a float value')
    else:
        if args.train_prop > 1:
            raise ValueError('Training data proportion must be between 0 and 1')

    if args.batch_size < 1:
        raise ValueError('Batch size must be an int and > 1')
    if args.num_epochs < 1:
        raise ValueError('Number of epochs must be an int and > 1')
    if args.batch_factor < 1:
        raise ValueError('Batch factor must be an int and > 1')
    if args.early_stop_val < 1 or args.early_stop_val > args.num_epochs:
        raise ValueError('Early stopping patience value must be an int and > 1 and < number of epochs')
    if args.aug_factor < 1:
        raise ValueError('Augmentation factor must be an int and > 1')
    # if args.cp_save_type == 'everyN':
    #     if args.cp_everyn_N < 1 or args.cp_everyn_N > args.num_epochs:
    #         raise ValueError(
    #             'N value for saving checkpoints for every N epochs must be an int and > 1and < number of epochs')

    if args.num_classes < 1:
        raise ValueError('Number of classes to consider in target segmentations must be an int and > 1')

    if args.resume_from_fold > args.cv_fold:
        raise ValueError('The fold to resume CV cannot be higher than the total number of folds specified!')

    with open(masterfile, 'r') as mfile:
        masterfile_content = (line.rstrip() for line in mfile)  # All lines including the blank ones
        masterfile_content = [line for line in masterfile_content if line]  # Removes blank lines

    header = masterfile_content[0]
    masterfile_content = masterfile_content[1:]

    keyword_dict = {'flair_kwords':['FLAIR', 'flair', 'Flair'],
                    't1_kwords': ['T1', 't1'],
                    'label_kwords': ['manualmask', 'manual_mask', 'Manualmask', 'Manual_mask', 'ManualMask'],
                    'ventdist_kwords': ['ventdistmap', 'ventdist_map', 'Ventdist_map', 'vent_dist_map',
                       'VentDistMap', 'Vent_dist_map'],
                    'GMdist_kwords': ['GMdistmap', 'GMdist_map', 'gmdist_map', 'GM_dist_map', 'gm_dist_map', 'GMDistMap']}

    keyword_keys = ['flair_kwords', 't1_kwords', 'label_kwords', 'ventdist_kwords', 'GMdist_kwords']

    # Order: FLAIR, T1, label, ventdist, GMdist
    inp_flags = [0, 0, 0, 0, 0]
    pos_array = [-1, -1, -1, -1, -1]

    input_kwords = header.split(' ')
    for column_id, kword in enumerate(input_kwords):
        for ki in range(5):
            if kword in keyword_dict[keyword_keys[ki]]:
                inp_flags[ki] += 1
                pos_array[ki] = column_id

    if any(inp_flags) > 1:
        raise ValueError('Keywords cannot be repeated in the header of the Masterfile')

    if inp_flags[0] == 0 and inp_flags[1] == 0:
        raise ValueError('At least FLAIR or T1 must be provided in the masterfile')

    if inp_flags[2] == 0:
        raise ValueError('Manualmask keyword is mandatory for training truenet.')

    numchannels = inp_flags[0] + inp_flags[1]

    if args.loss_function == 'weighted':
        weighted = True
    else:
        weighted = False

    if args.loss_function == 'weighted':
        if inp_flags[3] == 0 or inp_flags[4] == 0:
            raise ValueError('Masterfile must have both GMdistmap and ventdistmap keywords if '
                             '-loss is "weighted"!')

    filepath_keys = ['flair_path', 't1_path', 'gt_path', 'ventdist_path', 'gmdist_path', 'basename']

    files_indices = np.where(np.array(inp_flags) > 0)[0]

    # Create a list of dictionaries containing required filepaths for the training subjects for CV
    subj_name_dicts = []
    for line in range(len(masterfile_content)):
        paths = masterfile_content[line].split(' ')
        subj_name_dict = dict.fromkeys(filepath_keys)
        if len(paths) != files_indices.shape[0]:
            raise ValueError('No. of columns in line ' + str(line + 2) + ' does not match no. of keywords in the Masterfile header')
        count = 0
        for idx in range(5):
            if inp_flags[idx] > 0:
                if os.path.isfile(paths[count]):
                    subj_name_dict[filepath_keys[idx]] = paths[pos_array[idx]]
                    count += 1
                else:
                    raise ValueError(paths[idx] + 'is not a valid filename')
            else:
                subj_name_dict[filepath_keys[idx]] = None
        if inp_flags[0] > 0:
            basename = os.path.basename(paths[pos_array[0]]).split('.')[0]
        else:
            basename = os.path.basename(paths[pos_array[1]]).split('.')[0]
        subj_name_dict['basename'] = basename
        subj_name_dicts.append(subj_name_dict)

    if len(subj_name_dicts) < args.cv_fold:
        raise ValueError('Number of folds is greater than number of subjects!')

    # Create the loo_validate parameters dictionary
    cv_params = {'Learning_rate': args.init_learng_rate,
                 'fold': args.cv_fold,
                 'res_fold': args.resume_from_fold,
                 'Optimizer': args.optimizer,
                 'Epsilon': args.epsilon,
                 'Momentum': args.momentum,
                 'LR_Milestones': args.lr_sch_mlstone,
                 'LR_red_factor': args.lr_sch_gamma,
                 'Acq_plane': args.acq_plane,
                 'Num_channels': numchannels,
                 'Train_prop': args.train_prop,
                 'Batch_size': args.batch_size,
                 'Num_epochs': args.num_epochs,
                 'Batch_factor': args.batch_factor,
                 'Patience': args.early_stop_val,
                 'Aug_factor': args.aug_factor,
                 'Nclass': args.num_classes,
                 'EveryN': args.cp_everyn_N,
                 'SaveResume': args.save_resume_training,
                 'Use_CPU': args.use_cpu
                 }

    if args.save_full_model == 'True':
        save_wei = False
    else:
        save_wei = True

    if args.cp_save_type not in ['best', 'last', 'everyN']:
        raise ValueError('Invalid option for checkpoint save type: Valid options: best, last, everyN')

    if args.cp_save_type == 'everyN':
        if args.cp_everyn_N is None:
            raise ValueError('-cp_n must be provided to specify the epoch for loading CP when using -cp_'
                             'type is "everyN"!')

    print(save_wei)
    print(args.save_full_model)
    print(args.save_resume_training)

    # Cross-validation main function call
    truenet_cross_validate.main(subj_name_dicts, cv_params, aug=args.data_augmentation, weighted=weighted,
                                intermediate=args.intermediate, save_cp=args.save_checkpoint, save_wei=save_wei,
                                save_case=args.cp_save_type, verbose=args.verbose, dir_cp=out_dir,
                                output_dir=out_dir)

