# Triplanar U-Net ensemble network (TrUE-Net) model 

## DL tool for white matter hyperintensities segmentation

### Articles currently available at: https://doi.org/10.1016/j.media.2021.102184, https://doi.org/10.1016/j.media.2021.102215 & 10.1109/ISBI52829.2022.9761539

#### Software versions used for truenet:
- Python > 3.6
- PyTorch=1.5.0

#### Dependencies for prepare_truenet_data:
- FMRIB software library (FSL) 6.0

## TrUE-Net architecture:
<img
src="images/main_architecture_final.png"
alt="Triplanar U-Net ensemble network (TrUE-Net). (a) U-Net model used in individual planes, (b) Overall TrUE-Net architecture."
/>

### Applying spatial weights in the loss function:
We used a weighted sum of the voxel-wise cross-entropy loss function and the Dice loss as the total cost function. We weighted the CE loss function using a spatial weight map (a sample shown in the figure) to up-weight the areas that are more likely to contain the less represented class (i.e. WMHs).
<p align="center">
       <img
       src="images/spatial_weight_map.png"
       alt="Spatial weight maps to be applied in the truenet loss function."
       width=600
       />
</p>

## To install the truenet tool
Clone the git repository into your loacal directory and run:
``` 
python setup.py install
```
To find about the subcommands available in truenet:
```
truenet --help
```
And for options and inputs for each sub-command, type:
```
truenet <subcommand> --help (e.g. truenet train --help)
```
## Preprocessing and preparing masterfile for truenet
We used both T1-weighted and FLAIR images as inputs for the model. We reoriented the images to the standard MNI space, performed skull-stripping FSL BET and bias field correction using FSL FAST. We registered the T1-weighted image to the FLAIR using linear rigid-body registration.

The masterfile should have headers such as FLAIR T1 manualmask ventdistmap GMdistmap (as required) and filepaths in the corresponding order for one subject per row. A sample masterfile content with 5 subjects with FLAIR and T1 modalities is shown below:

```
FLAIR T1 manualmask ventdistmap GMdistmap
/path/to/the/file/subj1_FLAIR.nii.gz /path/to/the/file/subj1_T1.nii.gz /path/to/the/file/subj1_manualmask.nii.gz /path/to/the/file/subj1_vdistmap.nii.gz /path/to/the/file/subj1_gdistmap.nii.gz
/path/to/the/file/subj2_FLAIR.nii.gz /path/to/the/file/subj2_T1.nii.gz /path/to/the/file/subj2_manualmask.nii.gz /path/to/the/file/subj2_vdistmap.nii.gz /path/to/the/file/subj2_gdistmap.nii.gz
/path/to/the/file/subj3_FLAIR.nii.gz /path/to/the/file/subj3_T1.nii.gz /path/to/the/file/subj3_manualmask.nii.gz /path/to/the/file/subj3_vdistmap.nii.gz /path/to/the/file/subj3_gdistmap.nii.gz
/path/to/the/file/subj4_FLAIR.nii.gz /path/to/the/file/subj4_T1.nii.gz /path/to/the/file/subj4_manualmask.nii.gz /path/to/the/file/subj4_vdistmap.nii.gz /path/to/the/file/subj4_gdistmap.nii.gz
/path/to/the/file/subj5_FLAIR.nii.gz /path/to/the/file/subj5_T1.nii.gz /path/to/the/file/subj5_manualmask.nii.gz /path/to/the/file/subj5_vdistmap.nii.gz /path/to/the/file/subj5_gdistmap.nii.gz
```

Running **prepare_truenet_data** with unprocessed FLAIR and T1 images performs preprocessing on input images, computes distance maps (unless -nodistmaps flag is provided) and writes the filenames in a masterfile (if the masterfilename is provided as input). If the masterfile doesn't exist, a new file is created and headers will be added. If a masterfile already exists, the command appends the appropriate filepaths (indicated by the header in the existing masterfile) as an additional row in the end of the file.

The **prepare_truenet_data** command is used only if you need to **preprocess** images and write the filepaths in the masterfile. If you already have preprocessed images, you can write the masterfile yourself in the above mentioned format by **echo** command in a shell script.

The **prepare_truenet_data** command also generates the **white matter mask** from a dilated and inverted cortical CSF tissue segmentation (combined with other deep grey exclusion masks, using FSL FAST), similar to the **make_bianca_mask** command output in FSL BIANCA (Griffanti et al., 2016). You may later apply these masks to the truenet outputs generated.

#### prepare_truenet_data
```
Usage: prepare_truenet_data --FLAIR=<FLAIR_image_name> --T1=<T1_image_name> --outname=<output_basename> --masterfile=<input_masterfile_name> [--manualmask=<manualmask_name] [--nodistmaps] [--keepintermediate] [-v]"

The script applies the preprocessing pipeline on FLAIR, T1 and WM mask to be used in FSL truenet with a specified output basename
FLAIR_image_name 		= 	absolute/relative path of the input unprocessed FLAIR image with the nifti file
T1_image_name 		= 	absolute/relative path of the input unprocessed T1 image with the nifti file
output_basename 		= 	absolute/relative path for the processed FLAIR and T1 images; output_basename_FLAIR.nii.gz, output_basename_T1.nii.gz etc. will be saved
input_masterfile_name 	= 	absolute/relative path for the input masterfile with .txt file extension
manualmask_name 		= 	absolute/relative path of the manual mask with the nifti file (only mandatory for creating masterfiles for training truenet)

Note: If FLAIR is provided, please provide manualmask in FLAIR space.
specify --nodistmaps if you want to skip adding distance maps in the masterfile (distmaps are only mandatory for creating masterfiles for training truenet)
specify --keepintermediate if you want to save intermediate results
-v for verbose
```

## Running truenet

Triplanar ensemble U-Net model, v1.0.1

```  
Subcommands available:
    - truenet train         Training a TrUE-Net model from scratch
    - truenet apply         Applying a saved/pretrained TrUE-Net model for testing
    - truenet fine_tune     Fine-tuning a saved/pretrained TrUE-Net model from scratch
    - truenet loo_validate  Leave-one-out validation of TrUE-Net model
```

### Training the TrUE-Net model

#### truenet train: training the TrUE-Net model from scratch, v1.0.1

```
Usage: truenet train -i <input_masterfile> -m <model_directory> [options] 


Compulsory arguments:
       -i, --inp_file                      Name of the masterfile with the absolute path
       -m, --model_dir                     Directory for saving model weights

Optional arguments:
       -tr_prop, --train_prop              Proportion of data used for training [0, 1]. The rest will be used for validation [default = 0.8]
       -bfactor, --batch_factor            Number of subjects to be considered for each mini-epoch [default = 10]
       -loss, --loss_function              Applying spatial weights to loss function. Options: weighted, nweighted [default=weighted]
       -nclass, --num_classes              Number of classes to consider in the target labels (nclass=2 will consider only 0 and 1 in labels;
                                           any additional class will be considered part of background class [default = 2]
       -plane, --acq_plane                 The plane in which the model needs to be trained. Options: axial, sagittal, coronal, all [default = all]
       -da, --data_augmentation            Applying data augmentation [default = True]
       -af, --aug_factor                   Data inflation factor for augmentation [default = 2]
       -ilr, --init_learng_rate            Initial LR to use in scheduler [0, 0.1] [default=0.001]
       -lrm, --lr_sch_mlstone              Milestones for LR scheduler (e.g. -lrm 5 10 - to reduce LR at 5th and 10th epochs) [default = 10]
       -gamma, --lr_sch_gamma              Factor by which the LR needs to be reduced in the LR scheduler [default = 0.1]
       -opt, --optimizer                   Optimizer used for training. Options:adam, sgd [default = adam]
       -eps, --epsilon                     Epsilon for adam optimiser (default=1e-4)
       -mom, --momentum                    Momentum for sgd optimiser (default=0.9)
       -bs, --batch_size                   Batch size used for training [default = 8]
       -ep, --num_epochs                   Number of epochs for training [default = 60]
       -es, --early_stop_val               Number of epochs to wait for progress (early stopping) [default = 20]
       -sv_resume, --save_resume_training  Whether to save and resume training in case of interruptions [default = False]                        
       -sv_mod, --save_full_model          Saving the whole model instead of weights alone [default = False]
       -cv_type, --cp_save_type            Checkpoint to be saved. Options: best, last, everyN [default = last]
       -cp_n, --cp_everyn_N                If -cv_type=everyN, the N value [default = 10]
       -cpu, --use_cpu                     Force truenet to run on CPU even if GPU is detected (default=False)
       -v, --verbose                       Display debug messages [default = False]
       -h, --help.                           Print help message
```

### Testing the TrUE-Net model

### The pretrained models on MWSC and UKBB are currently available at https://drive.google.com/drive/folders/1iqO-hd27NSHHfKun125Rt-2fh1l9EiuT?usp=share_link

#### truenet evaluate: evaluating the TrUE-Net model, v1.0.1

```
Usage: truenet apply -i <input_masterfile> -m <model_name> -o <output_directory> [options]
   
Compulsory arguments:
       -i, --inp_file                        Name of the masterfile with the absolute path
       -m, --model_name                      Model basename with absolute path / 
                                             pretrained model name (mwsc, mwsc_flair, mwsc_t1, ukbb, ukbb_flair, ukbb_t1)
       -o, --output_dir                      Path to the directory for saving output predictions\n'
   
Optional arguments:
       -int, --intermediate                  Saving intermediate prediction results (individual planes) for each subject [default = False]
       -cpu, --use_cpu                       Force truenet to run on CPU even if GPU is detected (default=False) 
       -v, --verbose                         Display debug messages [default = False]
       -h, --help.                           Print help message
```

### Fine-tuning the TrUE-Net model

#### truenet fine_tune: training the TrUE-Net model from scratch, v1.0.1
<p align="center">
       <img
       src="images/fine_tuning_images.png"
       alt="Layers for fine-tuning truenet model."
       width=500
       />
</p>

```
Usage: truenet fine_tune -i <input_masterfile> -m <model_name> -o <output_directory> [options]

Compulsory arguments:
       -i, --inp_file                        Name of the masterfile with the absolute path
       -m, --model_name                      Model basename with absolute path / 
                                                   pretrained model name (mwsc, mwsc_flair, mwsc_t1, ukbb, ukbb_flair, ukbb_t1)
       -o, --output_dir                      Output directory for saving fine-tuned models/weights
    
Optional arguments:
       -ftlayers, --ft_layers                Layers to fine-tune starting from the decoder (e.g. 1 2 -> final two decoder layers) (see the figure above)
       -tr_prop, --train_prop                Proportion of data used for fine-tuning [0, 1]. The rest will be used for validation [default = 0.8]
       -bfactor, --batch_factor              Number of subjects to be considered for each mini-epoch [default = 10]
       -loss, --loss_function                Applying spatial weights to loss function. Options: weighted, nweighted [default=weighted]
       -nclass, --num_classes                Number of classes to consider in the target labels (nclass=2 will consider only 0 and 1 in labels; 
                                             any additional class will be considered part of background class [default = 2]
       -plane, --acq_plane                   The plane in which the model needs to be fine-tuned. Options: axial, sagittal, coronal, all [default=all]
       -da, --data_augmentation              Applying data augmentation [default = True]
       -af, --aug_factor                     Data inflation factor for augmentation [default = 2]
       -ilr, --init_learng_rate              Initial LR to use in scheduler for fine-tuning [0, 0.1] [default=0.0001]
       -lrm, --lr_sch_mlstone                Milestones for LR scheduler (e.g. -lrm 5 10 - to reduce LR at 5th and 10th epochs) [default = 10]
       -gamma, --lr_sch_gamma                Factor by which the LR needs to be reduced in the LR scheduler [default = 0.1]
       -opt, --optimizer                     Optimizer used for fine-tuning. Options:adam, sgd [default = adam]
       -eps, --epsilon                       Epsilon for adam optimiser (default=1e-4)
       -mom, --momentum                      Momentum for sgd optimiser (default = 0.9)   
       -bs, --batch_size                     Batch size used for fine-tuning [default = 8]
       -ep, --num_epochs                     Number of epochs for fine-tuning [default = 60]
       -es, --early_stop_val                 Number of fine-tuning epochs to wait for progress (early stopping) [default = 20]
       -sv_resume, --save_resume_training    Whether to save and resume training in case of interruptions (default-False)
       -sv_mod, --save_full_model            Saving the whole fine-tuned model instead of weights alone [default = False]
       -cv_type, --cp_save_type              Checkpoint to be saved. Options: best, last, everyN [default = last]
       -cp_n, --cp_everyn_N                  If -cv_type = everyN, the N value [default = 10]
       -cpu, --use_cpu                       Force truenet to run on CPU even if GPU is detected (default=False)
       -v, --verbose                         Display debug messages [default = False]
       -h, --help.                           Print help message
```

### Cross-validation of TrUE-Net model

#### truenet cross_validate: cross-validation of the TrUE-Net model, v1.0.1  
   
```
Usage: truenet cross_validate -i <input_masterfile> -o <output_directory> [options]
   
Compulsory arguments:
       -i, --inp_file                         Name of the masterfile with the absolute path
       -o, --output_dir                      Output directory for saving predictions (and models)
   
Optional arguments:\n'
       -fold, --cv_fold                      Number of folds for crossvalidation (default = 5)
       -resume_fold, --resume_from_fold      Resume crossvalidation from the specified fold (default = 1)
       -tr_prop, --train_prop                Proportion of data used for training [0, 1]. The rest will be used for validation [default = 0.8]
       -bfactor, --batch_factor              Number of subjects to be considered for each mini-epoch [default = 10]
       -loss, --loss_function                Applying spatial weights to loss function. Options: weighted, nweighted [default=weighted]
       -nclass, --num_classes                Number of classes to consider in the target labels (nclass=2 will consider only 0 and 1 in labels; 
                                             any additional class will be considered part of background class [default = 2]
       -plane, --acq_plane                   The plane in which the model needs to be trained. Options: axial, sagittal, coronal, all [default = all]
       -da, --data_augmentation              Applying data augmentation [default = True]
       -af, --aug_factor                     Data inflation factor for augmentation [default = 2]
       -ilr, --init_learng_rate              Initial LR to use in scheduler for training [0, 0.1] [default=0.0001]
       -lrm, --lr_sch_mlstone                Milestones for LR scheduler (e.g. -lrm 5 10 - to reduce LR at 5th and 10th epochs) [default = 10]
       -gamma, --lr_sch_gamma                Factor by which the LR needs to be reduced in the LR scheduler [default = 0.1]
       -opt, --optimizer                     Optimizer used for training. Options:adam, sgd [default = adam]
       -eps, --epsilon                       Epsilon for adam optimiser (default=1e-4)
       -mom, --momentum                      Momentum for sgd optimiser (default=0.9)
       -bs, --batch_size                     Batch size used for fine-tuning [default = 8]
       -ep, --num_epochs                     Number of epochs for fine-tuning [default = 60]
       -es, --early_stop_val                 Number of fine-tuning epochs to wait for progress (early stopping) [default = 20]
       -int, --intermediate                  Saving intermediate prediction results (individual planes) for each subject [default = False]
       -sv_resume, --save_resume_training    Whether to save and resume training in case of interruptions (default-False)
       -sv_mod, --save_full_model            Saving the whole fine-tuned model instead of weights alone [default = False]
       -cv_type, --cp_save_type              Checkpoint to be saved. Options: best, last, everyN [default = last]
       -cp_n, --cp_everyn_N                  If -cv_type = everyN, the N value [default = 10]
       -cpu, --use_cpu                       Force truenet to run on CPU even if GPU is detected (default=False)
       -v, --verbose                         Display debug messages [default = False]
       -h, --help.                           Print help message
```

If you use the tool from this repository, please cite the following papers:

- Sundaresan, V., Zamboni, G., Rothwell, P.M., Jenkinson, M. and Griffanti, L., 2021. Triplanar ensemble U-Net model for white matter hyperintensities segmentation on MR images. Medical Image Analysis, p.102184. [DOI: https://doi.org/10.1016/j.media.2021.102184] (preprint available at https://doi.org/10.1101/2020.07.24.219485)
- Sundaresan, V., Zamboni, G., Dinsdale, N. K., Rothwell, P. M., Griffanti, L., & Jenkinson, M. 2021. Comparison of domain adaptation techniques for white matter hyperintensity segmentation in brain MR images. Medical image analysis, 74, p.102215. [DOI: https://doi.org/10.1016/j.media.2021.102215] (preprint available at https://doi.org/10.1101/2021.03.12.435171).
- Sundaresan, V., Dinsdale, N. K., Jenkinson, M. and Griffanti, L., 2022. Omni-supervised domain adversarial training for white matter hyperintensity segmentation in the UK Biobank. IEEE ISBI 2022 [DOI: 10.1109/ISBI52829.2022.9761539]. 



